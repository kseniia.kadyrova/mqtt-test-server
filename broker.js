"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mosca_1 = require("mosca");
var mqtt_1 = require("mqtt");
var brokerSettings = {
    port: 1883,
};
var server = new mosca_1.Server(brokerSettings, function () { });
// This is our MQTT client that the broker will use to publish messages
var clientOptions = {
    host: 'localhost',
    port: 1883,
};
var client = (0, mqtt_1.connect)(clientOptions);
server.on('clientConnected', function (mqttClient) {
    console.log('client connected', mqttClient.id);
});
server.on('published', function (packet, mqttClient) {
    console.log("Received: topic: ".concat(packet.topic, "  Message: ").concat(packet.payload.toString()));
    // If the message is from 'some/specific/topic', broker publishes a response
    if (packet.topic === 'CardTerminal') {
        var pubOptions = {
            qos: 0,
            retain: false,
        };
        // Get transactionId from payload
        var transactionId = JSON.parse(packet.payload.toString()).transactionId;
        // Here, we publish a response to the topic 'TransactionID.12345'
        client.publish("TransactionID.".concat(transactionId), 'This is a response from the broker.', pubOptions);
    }
});
server.on('ready', function () {
    console.log('Broker is up and running');
    // Here, you can also initiate some broker's periodic publications if needed
    setInterval(function () {
        var pubOptions = {
            qos: 0,
            retain: false,
        };
        client.publish('broker/status', 'Broker is alive and well', pubOptions);
    }, 30000); // Sends a broker status every 10 seconds
});
