import { Server } from 'mosca';
import { MqttClient, IClientOptions, IClientPublishOptions, connect } from 'mqtt';

const brokerSettings: { port: number } = {
    port: 1883,
};

const server = new Server(brokerSettings, () => {});

// This is our MQTT client that the broker will use to publish messages
const clientOptions: IClientOptions = {
    host: 'localhost',
    port: 1883,
};
const client: MqttClient = connect(clientOptions);

server.on('clientConnected', (mqttClient) => {
    console.log('client connected', mqttClient.id);
});

server.on('published', (packet, mqttClient: MqttClient) => {
    console.log(`Received: topic: ${packet.topic}  Message: ${packet.payload.toString()}`);

    // If the message is from 'some/specific/topic', broker publishes a response
    if (packet.topic === 'CardTerminal') {
        const pubOptions: IClientPublishOptions = {
            qos: 0,
            retain: false,
        };
        // Get transactionId from payload
        const { transactionId } = JSON.parse(packet.payload.toString());
        // Here, we publish a response to the topic 'TransactionID.12345'
        client.publish(`Transaction/${transactionId}`, 'This is a response from the broker.', pubOptions);
    }
});

server.on('ready', () => {
    console.log('Broker is up and running');

    // Here, you can also initiate some broker's periodic publications if needed
    setInterval(() => {
        const pubOptions: IClientPublishOptions = {
            qos: 0,
            retain: false,
        };
        client.publish('broker/status', 'Broker is alive and well', pubOptions);
    }, 30000); // Sends a broker status every 10 seconds
});

